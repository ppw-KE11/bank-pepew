from django.urls import path
from . import views

app_name = 'appKl'

urlpatterns = [
    path('', views.index, name='index'),
]