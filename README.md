## Anggota KE11:
1. Hamam Abdurrachman
2. Muhammad Agung Yulianang
3. Safira Rizki Anartya
4. Samratu Maharani

## Link herokuapp:
[http://bank-pepew.herokuapp.com/]

## Background
Web ini kami desain sebagai layanan internet banking yang menyediakan berbagai fitur diantaranya, fitur transfer uang, fitur pembayaran/pembelian online, dan juga fitur pinjam uang ke bank. Fitur transfer uang yaitu fitur yang dapat digunakan oleh user untuk mentransfer uang ke bank lain. Fitur Pembayaran/Pembelian terbagi menjadi berbagai fitur lagi yaitu user dapat membeli token listrik, bayar kuliah, Virtual Account Billing, dan fitur untuk melunasi pinjaman. Fitur pinjaman menyediakan layanan pinjam uang ke bank, dengan harus memberikan alasan mengapa user ingin meminjam uang. User harus menunggu apakah pinjaman sudah dikonfirmasi, selama bank sedang mengecek alasan pinjaman. Nantinya, bank akan memberikan konfirmasi apakah alasan user diterima atau tidak.

## Fitur:
1. Fitur Homepage
2. Fitur Transfer
3. Fitur Pembayaran, yang terbagi lagi menjadi 4 fitur, yaitu:
	- Fitur beli token listrik
	- Fitur bayar kuliah
	- Fitur Virtual Account Billing
	- Fitur lunasi pinjaman
4. Fitur pinjam uang ke bank

[![pipeline](https://gitlab.com/ppw-KE11/bank-pepew/badges/master/pipeline.svg)](https://bank-pepew.herokuapp.com/)